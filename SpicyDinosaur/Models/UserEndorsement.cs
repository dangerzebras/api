﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpicyDinosaur.Models
{
    public class UserEndorsement
    {
        [BsonElement("userId")]
        public string UserId { get; set; }
        [BsonElement("userName")]
        public string UserName { get; set; }
        [BsonElement("date")]
        public DateTime EndorseDate { get; set; }
    }
}
