﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpicyDinosaur.Models
{
    public class SkillEndorsement
    {
        [BsonElement("skillId")]
        public string SkillId { get; set; }
        [BsonElement("endorsements")]
        public List<UserEndorsement> Endorsements { get; set; }
    }
}
