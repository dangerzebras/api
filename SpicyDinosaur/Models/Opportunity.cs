﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpicyDinosaur.Models
{
    public class Opportunity
    {
        [BsonElement("_id")]
        public string Id { get; set; }
        [BsonElement("title")]
        public string Title { get; set; }
        [BsonElement("description")]
        public string Description { get; set; }
        [BsonElement("addedDate")]
        public DateTime AddedDate { get; set; }
        [BsonElement("skills")]
        public string[] SkillIds { get; set; }
        [BsonElement("user_ids")]
        public List<string> UserIds { get; set; }
    }
}
