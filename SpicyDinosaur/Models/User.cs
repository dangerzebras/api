﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpicyDinosaur.Models
{
    public class User
    {
        [BsonElement("_id")]
        public object Id { get; set; }
        [BsonElement("userName")]
        public string UserName { get; set; }
        [BsonElement("userId")]
        public string UserId { get; set; }
        [BsonElement("skills")]
        public List<SkillEndorsement> Skills { get; set; }
        public string Name { get; set; } // needed for message action response object
    }
}
