﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpicyDinosaur.Models
{
    public class Skill
    {
        [BsonElement("_id")]
        public string Id { get; set; }
        [BsonElement("skillName")]
        public string SkillName { get; set; }
    }
}
