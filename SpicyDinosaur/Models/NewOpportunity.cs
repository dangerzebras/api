﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpicyDinosaur.Models
{
    public class NewOpportunity
    {
        [BsonElement("title")]
        public string Title { get; set; }
        [BsonElement("description")]
        public string Description { get; set; }
        [BsonElement("autoPost")]
        public bool AutoPost { get; set; }
        [BsonElement("skills")]
        public string[] SkillIds { get; set; }
    }
}
