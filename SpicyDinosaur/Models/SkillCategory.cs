﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpicyDinosaur.Models
{
    public class SkillCategory
    {
        [BsonElement("_id")]
        public object Id { get; set; }
        [BsonElement("categoryName")]
        public string CategoryName { get; set; }
        [BsonElement("skills")]
        public Skill[] Skills { get; set; }
    }
}
