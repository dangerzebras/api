﻿namespace SpicyDinosaur.Models.SlackResponses
{
    public class Team
    {
        public string Id { get; set; }
        public string Domain { get; set; }
    }
}