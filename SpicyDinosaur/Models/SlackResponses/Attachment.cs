﻿using System.Collections.Generic;

namespace SpicyDinosaur.Models
{
    public class Attachment
    {
        public string Title { get; set; }
        public List<Field> Fields { get; set; }
        public string Text { get; set; }
        public string Fallback { get; set; }
        public string Callback_id { get; set; }
        public string Color { get; set; }
        public string Attachment_type { get; set; }
        public List<Action> Actions { get; set; }
        public string Author_name { get; set; }
        public string Author_icon { get; set; }
        public string Image_url { get; set; }
    }
}