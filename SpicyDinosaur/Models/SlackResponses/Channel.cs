﻿namespace SpicyDinosaur.Models.SlackResponses
{
    public class Channel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}