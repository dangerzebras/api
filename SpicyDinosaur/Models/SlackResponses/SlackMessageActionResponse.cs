﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpicyDinosaur.Models.SlackResponses
{
    public class SlackMessageActionResponse
    {
        public string payload { get; set; }
    }
}
