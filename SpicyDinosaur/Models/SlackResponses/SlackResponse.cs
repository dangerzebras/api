﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpicyDinosaur.Models
{
    public class SlackResponse
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public ResponseType Response_type { get; set; }
        public string Text { get; set; }
        public List<Attachment> Attachments { get; set; }
    }
}
