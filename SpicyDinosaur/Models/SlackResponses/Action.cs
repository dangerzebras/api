﻿namespace SpicyDinosaur.Models
{
    public class Action
    {
        public string Name { get; set; }
        public string Text { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }
    }
}