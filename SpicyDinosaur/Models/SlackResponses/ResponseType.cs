﻿namespace SpicyDinosaur.Models
{
    public enum ResponseType
    {
        ephemeral,
        in_channel
    }
}