﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpicyDinosaur.Models.SlackResponses
{
    public class Payload
    {
        public List<Action> Actions { get; set; }
        public string Callback_id { get; set; }
        public Team Team { get; set; }
        public Channel Channel { get; set; }
        public User User { get; set; }
        public string Action_ts { get; set; }
        public string Message_ts { get; set; }
        public string Attachment_id { get; set; }
        public string Token { get; set; }
        public OriginalMessage Original_message { get; set; }
        public string Response_url { get; set; }
        public string Trigger_id { get; set; }
    }
}
