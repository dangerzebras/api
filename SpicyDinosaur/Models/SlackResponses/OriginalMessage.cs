﻿using System.Collections.Generic;

namespace SpicyDinosaur.Models.SlackResponses
{
    public class OriginalMessage
    {
        public string Text { get; set; }
        public List<Attachment> Attachments { get; set; }
    }
}