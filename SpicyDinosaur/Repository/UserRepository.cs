﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using SpicyDinosaur.Models;
using SpicyDinosaur.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Threading.Tasks;

namespace SpicyDinosaur.Repository
{
    public class UserRepository: IUserRepository
    {
        private readonly MongoClient mongoClient;
        private readonly IMongoDatabase database;
        public UserRepository()
        {
            string connectionString =
  @"mongodb://seapanda:ifw0ZecLbG8rjflLAl9l2Eg4R1Fo7sBfVsAxPFZWDISefMUIKf0MvHfUt08La5FFyq0kc0V0uy0cuHPHuSKVXQ==@seapanda.documents.azure.com:10255/?ssl=true&replicaSet=globaldb";
            MongoClientSettings settings = MongoClientSettings.FromUrl(
              new MongoUrl(connectionString)
            );
            settings.SslSettings =
              new SslSettings() { EnabledSslProtocols = SslProtocols.Tls12 };
            this.mongoClient = new MongoClient(settings);
            this.database = mongoClient.GetDatabase("skills");
        }

        public async Task<IList<User>> GetUsersWithSkills()
        {
            var defaultCollection = database.GetCollection<User>("users");
            return await defaultCollection.Find(_ => true).ToListAsync();
        }

        public async Task<User> Endorse(string userId, string userName, string skillId, UserEndorsement userEndorsement)
        {
            var defaultCollection = database.GetCollection<User>("users");
            var user = await defaultCollection.Find(_ => _.UserId == userId).FirstOrDefaultAsync();
            if (user == null)
            {
                user = new User
                {
                    Id = Guid.NewGuid().ToString(),
                    UserId = userId,
                    UserName = userName,
                    Skills = new List<SkillEndorsement>
                    {
                        new SkillEndorsement
                        {
                            SkillId = skillId,
                            Endorsements = new List<UserEndorsement>
                            {
                                userEndorsement
                            }
                        }
                    }
                };
                defaultCollection.InsertOne(user);
            } else
            {
                var skillEndorsement = user.Skills.Find(s => s.SkillId == skillId);
                if (skillEndorsement == null)
                {
                    user.Skills.Add(new SkillEndorsement
                    {
                        SkillId = skillId,
                        Endorsements = new List<UserEndorsement>
                        {
                            userEndorsement
                        }
                    });
                } else
                {
                    var existingEndorsement = skillEndorsement.Endorsements.Find(e => e.UserId == userEndorsement.UserId);
                    if (existingEndorsement != null)
                    {
                        throw new Exception("You have already endorsed this user for this skill");
                    }
                    skillEndorsement.Endorsements.Add(userEndorsement);
                }
                await defaultCollection.ReplaceOneAsync(_ => _.UserId == userId, user);
            }
            return user;
        }

        public async Task<User> GetUserById(string userId)
        {
            var defaultCollection = database.GetCollection<User>("users");
            return await defaultCollection.Find(_ => _.UserId == userId).FirstOrDefaultAsync();
        }
    }
}
