﻿using MongoDB.Driver;
using SpicyDinosaur.Models;
using SpicyDinosaur.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Threading.Tasks;

namespace SpicyDinosaur.Repository
{
    public class SlackRequestRepository: ISlackRequestRepository
    {
        private readonly IMongoDatabase database;
        public SlackRequestRepository()
        {
            string connectionString =
  @"mongodb://seapanda:ifw0ZecLbG8rjflLAl9l2Eg4R1Fo7sBfVsAxPFZWDISefMUIKf0MvHfUt08La5FFyq0kc0V0uy0cuHPHuSKVXQ==@seapanda.documents.azure.com:10255/?ssl=true&replicaSet=globaldb";
            MongoClientSettings settings = MongoClientSettings.FromUrl(
              new MongoUrl(connectionString)
            );
            settings.SslSettings =
              new SslSettings() { EnabledSslProtocols = SslProtocols.Tls12 };
            var mongoClient = new MongoClient(settings);
            this.database = mongoClient.GetDatabase("skills");
        }

        public void AddLog(SlashCommandPayload slashCommand)
        {
            var defaultCollection = database.GetCollection<SlashCommandPayload>("slackLogs");
            defaultCollection.InsertOne(slashCommand);
        }
    }
}
