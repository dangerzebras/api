﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using SpicyDinosaur.Models;
using SpicyDinosaur.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Threading.Tasks;

namespace SpicyDinosaur.Repository
{
    public class SkillsRepository: ISkillsRepository
    {
        private readonly MongoClient mongoClient;
        private readonly IMongoDatabase database;
        private readonly IUserRepository userRepository;
        public SkillsRepository(IUserRepository userRepository)
        {
            string connectionstring =
  @"mongodb://seapanda:ifw0ZecLbG8rjflLAl9l2Eg4R1Fo7sBfVsAxPFZWDISefMUIKf0MvHfUt08La5FFyq0kc0V0uy0cuHPHuSKVXQ==@seapanda.documents.azure.com:10255/?ssl=true&replicaSet=globaldb";
            MongoClientSettings settings = MongoClientSettings.FromUrl(
              new MongoUrl(connectionstring)
            );
            settings.SslSettings =
              new SslSettings() { EnabledSslProtocols = SslProtocols.Tls12 };
            this.mongoClient = new MongoClient(settings);
            this.database = mongoClient.GetDatabase("skills");
            this.userRepository = userRepository;
        }

        public async Task<IList<SkillCategory>> GetSkillsByCategory()
        {
            var defaultCollection = database.GetCollection<SkillCategory>("defaultSkills");
            return await defaultCollection.Find(_ => true).ToListAsync();
        }

        public async Task<IList<Skill>> GetSkills()
        {
            var skillCategories = await GetSkillsByCategory();
            var skills = new List<Skill>();
            foreach(SkillCategory category in skillCategories)
            {
                skills.AddRange(category.Skills);
            }
            skills = skills.OrderBy(s => s.SkillName).ToList();
            return skills;
        }

        public async Task<Skill> GetSkillByName(string skillName)
        {
            var defaultCollection = database.GetCollection<SkillCategory>("defaultSkills");
            var skillCategory = await defaultCollection.Find(x => x.Skills.Any(t => t.SkillName.ToLower() == skillName.ToLower())).FirstOrDefaultAsync();
            return FindSkillByName(skillCategory.Skills, skillName);
        }

        public async Task<Skill> GetSkillById(string id)
        {
            var defaultCollection = database.GetCollection<SkillCategory>("defaultSkills");
            var skillCategory = await defaultCollection.Find(x => x.Skills.Any(t => t.Id == id)).FirstOrDefaultAsync();
            return FindSkillById(skillCategory.Skills, id);
        }

        public async Task<IList<string>> GetSkillNamesByIds(string[] ids)
        {
            var skills = await GetSkills();
            IList<string> skillNames = new List<string>();
            foreach (string id in ids)
            {
                var skill = skills.FirstOrDefault(x => x.Id == id);
                if (skill != null)
                {
                    skillNames.Add(skill.SkillName);
                }

            }

            return skillNames;
        }

        private Skill FindSkillByName(IList<Skill> skills, string skillName)
        {
            return skills.ToList().Find(x => x.SkillName.ToLower() == skillName.ToLower());
        }

        private Skill FindSkillById(IList<Skill> skills, string id)
        {
            return skills.ToList().Find(x => x.Id == id);
        }

        public async Task<SkillCategory> CreateSkillCategory(SkillCategory category)
        {
            category.Id = Guid.NewGuid().ToString();
            foreach(Skill skill in category.Skills)
            {
                skill.Id = Guid.NewGuid().ToString();
            }
            var defaultCollection = database.GetCollection<SkillCategory>("defaultSkills");
            await defaultCollection.InsertOneAsync(category);
            return category;
        }

        public async Task<IList<Opportunity>> GetOpportunities(int? count = null)
        {
            var defaultCollection = database.GetCollection<Opportunity>("opportunities");
            if (count == null)
            {
                return await defaultCollection.Find(_ => true).SortByDescending(x => x.AddedDate).ToListAsync();
            }
            return await defaultCollection.Find(_ => true).SortByDescending(x => x.AddedDate).Limit(count).ToListAsync();
        }

        public async Task<Opportunity> GetOpportunityById(string id)
        {
            var defaultCollection = database.GetCollection<Opportunity>("opportunities");
            return defaultCollection.Find(x => x.Id == id).FirstOrDefault();
        }

        public async Task<IList<Opportunity>> GetOpportunitiesBySkillId(string skillId)
        {
            var defaultCollection = database.GetCollection<Opportunity>("opportunities");
            return await defaultCollection.Find(x => x.SkillIds.Any(t => t == skillId)).ToListAsync();
        }

        public async Task<IList<Opportunity>> GetOpportunitiesBySkillIds(string[] skillIds)
        {
            var defaultCollection = database.GetCollection<Opportunity>("opportunities");
            return await defaultCollection.Find(x => x.SkillIds.Any(t => skillIds.Contains(t))).ToListAsync();
        }

        public async Task<Opportunity> CreateOpportunity(NewOpportunity opportunity)
        {
            var defaultCollection = database.GetCollection<Opportunity>("opportunities");
            var newOpportunity = new Opportunity
            {
                Id = Guid.NewGuid().ToString(),
                Title = opportunity.Title,
                Description = opportunity.Description,
                SkillIds= opportunity.SkillIds,
                AddedDate = DateTime.UtcNow
            };
            await defaultCollection.InsertOneAsync(newOpportunity);
            return newOpportunity;
        }

        public async Task<Opportunity> UpdateOpportunity(string userId, string opportunityId)
        {
            var defaultCollection = database.GetCollection<Opportunity>("opportunities");
            var opportunity = await GetOpportunityById(opportunityId);
            if (opportunity != null) // need to check this? 
            {
                if (opportunity.UserIds == null)
                {
                    opportunity.UserIds = new List<string>();
                }
                opportunity.UserIds.Add(userId);
                await defaultCollection.ReplaceOneAsync(_ => _.Id == opportunity.Id, opportunity);
            }
            return opportunity;
        }

        public async Task<Dictionary<string, int>> GetUserSkills(string userId)
        {
            var user = await userRepository.GetUserById(userId);
            var skillEndorsements = new Dictionary<string, int>();
            foreach (SkillEndorsement skillEndorsement in user.Skills)
            {
                var skill = await GetSkillById(skillEndorsement.SkillId);
                skillEndorsements.Add(skill.SkillName, skillEndorsement.Endorsements.Count);
            }
            return skillEndorsements;
        }
    }
}
