﻿using SpicyDinosaur.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpicyDinosaur.Repository.Interfaces
{
    public interface IUserRepository
    {
        Task<IList<User>> GetUsersWithSkills();
        Task<User> GetUserById(string userId);
        Task<User> Endorse(string userId, string userName, string skillId, UserEndorsement userEndorsement);
    }
}
