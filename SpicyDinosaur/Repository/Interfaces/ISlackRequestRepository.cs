﻿using SpicyDinosaur.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpicyDinosaur.Repository.Interfaces
{
    public interface ISlackRequestRepository
    {
        void AddLog(SlashCommandPayload slashCommand);
    }
}
