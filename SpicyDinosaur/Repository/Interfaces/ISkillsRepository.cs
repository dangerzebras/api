﻿using SpicyDinosaur.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpicyDinosaur.Repository.Interfaces
{
    public interface ISkillsRepository
    {
        Task<IList<SkillCategory>> GetSkillsByCategory();
        Task<IList<Skill>> GetSkills();
        Task<SkillCategory> CreateSkillCategory(SkillCategory category);
        Task<Skill> GetSkillByName(string skillName);
        Task<Skill> GetSkillById(string id);
        Task<IList<string>> GetSkillNamesByIds(string[] ids);
        Task<Opportunity> GetOpportunityById(string id);
        Task<IList<Opportunity>> GetOpportunities(int? count = null);
        Task<IList<Opportunity>> GetOpportunitiesBySkillId(string skillId);
        Task<IList<Opportunity>> GetOpportunitiesBySkillIds(string[] skillIds);
        Task<Opportunity> CreateOpportunity(NewOpportunity opportunity);
        Task<Opportunity> UpdateOpportunity(string userId, string opportunityId);
        Task<Dictionary<string, int>> GetUserSkills(string userId);
    }
}
