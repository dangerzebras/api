﻿using SpicyDinosaur.Models;
using SpicyDinosaur.Repository.Interfaces;
using SpicyDinosaur.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpicyDinosaur.Services
{
    public class UserService: IUserService
    {
        private readonly IUserRepository userRepository;
        private readonly ISkillsRepository skillsRepository;
        private readonly ISlackService slackService;

        public UserService(IUserRepository userRepository, ISkillsRepository skillsRepository, ISlackService slackService)
        {
            this.userRepository = userRepository;
            this.skillsRepository = skillsRepository;
            this.slackService = slackService;
        }
        public async Task Endorse(string userId, string userName, string slackInput, string responseUrl)
        {
            var slackInputArray = slackInput.Split("> ").ToList();
            if (slackInputArray.Count <= 1)
            {
                slackService.SendResponse(responseUrl, "Sorry, we can't save the endorsement, something is missing");
            }
            var userToEndorseArray = slackInputArray[0].Split("|");
            var userEndorsement = new UserEndorsement
            {
                UserId = userId,
                UserName = userName,
                EndorseDate = DateTime.UtcNow
            };
            var skill = await skillsRepository.GetSkillByName(slackInputArray[1]);
            if (skill == null)
            {
                slackService.SendResponse(responseUrl, "This skill doesn't exist. Please use /endorse for an available list of skills.");
            }
            try
            {
                await userRepository.Endorse(
                    userToEndorseArray[0].Remove(0,2), 
                    userToEndorseArray[1], 
                    skill.Id, 
                    userEndorsement);
                slackService.SendResponse(responseUrl, "Successfully endorsed!");
            } catch (Exception ex)
            {
                slackService.SendResponse(responseUrl, ex.Message);
            }

        }
    }
}
