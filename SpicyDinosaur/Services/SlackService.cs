﻿using Newtonsoft.Json;
using SpicyDinosaur.Models;
using SpicyDinosaur.Services.Interfaces;
using SpicyDinosaur.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace SpicyDinosaur.Services
{
    public class SlackService: ISlackService
    {
        private readonly HttpClient client;
        private static readonly string opportunitiesChannelWebhook = "https://hooks.slack.com/services/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";

        private readonly ISkillsRepository skillsRepository;

        public SlackService(ISkillsRepository skillsRepository)
        {
            this.skillsRepository = skillsRepository;
            client = new HttpClient();
        }

        public async void SendMessageToChannel(NewOpportunity opportunity)
        {
            if (opportunity.AutoPost)
            {
                var skills = await skillsRepository.GetSkillNamesByIds(opportunity.SkillIds);
                var textString = $"A new opportunity is available!\n*Title:* {opportunity.Title}\n*Description:* {opportunity.Description}\n*Skills:*\n";
                foreach(string skill in skills)
                {
                    textString += $"- {skill}\n";
                }
                SendResponse(opportunitiesChannelWebhook, textString);
            }
        }
        public async void SendHelpText(string responseUrl)
        {
            var skills = await skillsRepository.GetSkills();
            var skillString = $"Linked Out plugin for integrating skills and opportunities at Daugherty.\n\n" +
                $"*Commands:*\n" +
                $"`/endorse`\nLists the available skills.\n" +
                $"`/endorse [@someone] [skill]`\nEndorses someone for the specified skill.\n" +
                $"`/opportunities`\nLists the available opportunities.\n" +
                $"\n\n*Skills:*\n```";
            foreach (Skill skill in skills)
            {
                skillString += $"{skill.SkillName}, ";
            }
            skillString.Remove(skillString.Length - 2);
            skillString += "```";
            SendResponse(responseUrl, skillString);
        }

        public void SendResponse(string responseUrl, string responseData)
        {
            var jsonObject = new SlackMessage
            {
                text = responseData
            };
            var json = JsonConvert.SerializeObject(jsonObject);
            client.PostAsync(responseUrl, new StringContent(json));   
        }

        public async void SendUserSkills(string userId, string responseUrl)
        {
            var userSkills = await skillsRepository.GetUserSkills(userId);
            var skillString = "Your endorsed skills: \n";
            foreach (var skill in userSkills)
            {
                skillString += $"{skill.Key} - {skill.Value}\n";
            }
            SendResponse(responseUrl, skillString);
        }
    }
}
