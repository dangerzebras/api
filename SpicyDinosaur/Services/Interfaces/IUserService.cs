﻿using SpicyDinosaur.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpicyDinosaur.Services.Interfaces
{
    public interface IUserService
    {
        Task Endorse(string userId, string userName, string slackInput, string responseUrl);
    }
}

