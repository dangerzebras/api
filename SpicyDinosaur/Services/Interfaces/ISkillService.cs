﻿using SpicyDinosaur.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpicyDinosaur.Services.Interfaces
{
    public interface ISkillService
    {
        void SendSkillsList(string responseUrl);
        Task<Opportunity> CreateOpportunity(NewOpportunity opportunity);
    }
}
