﻿using SpicyDinosaur.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpicyDinosaur.Services.Interfaces
{
    public interface ISlackService
    {
        void SendResponse(string responseUrl, string responseData);
        void SendMessageToChannel(NewOpportunity opportunity);
        void SendHelpText(string responseUrl);
        void SendUserSkills(string userId, string responseUrl);
    }
}
