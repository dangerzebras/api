﻿using SpicyDinosaur.Models;
using SpicyDinosaur.Repository.Interfaces;
using SpicyDinosaur.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpicyDinosaur.Services
{
    public class SkillService : ISkillService
    {
        private readonly ISkillsRepository skillsRepository;
        private readonly ISlackService slackService;

        public SkillService(ISkillsRepository skillsRepository, ISlackService slackService)
        {
            this.skillsRepository = skillsRepository;
            this.slackService = slackService;
        }
        public async void SendSkillsList(string responseUrl)
        {
            var skills = await skillsRepository.GetSkills();
            var skillString = $"Pick one of the {skills.Count} skills available to endorse someone: \n";
            foreach(Skill skill in skills)
            {
                skillString += $"{skill.SkillName}\n";
            }
            slackService.SendResponse(responseUrl, skillString);
        }

        public async Task<Opportunity> CreateOpportunity(NewOpportunity opportunity)
        {
            var addedOpportunity = await skillsRepository.CreateOpportunity(opportunity);
            slackService.SendMessageToChannel(opportunity);
            return addedOpportunity;
        }
    }
}
