﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SpicyDinosaur.Repository.Interfaces;

namespace SpicyDinosaur.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserRepository userRepository;

        public UserController(IUserRepository userRepository)
        {
            this.userRepository = userRepository;
        }

        // GET api/values
        [HttpGet]
        public async Task<IActionResult> GetUsersWithSkills()
        {
            var users = await userRepository.GetUsersWithSkills();
            return Ok(users);
        }
    }
}