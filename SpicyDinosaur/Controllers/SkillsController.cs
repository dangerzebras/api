﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using SpicyDinosaur.Models;
using SpicyDinosaur.Repository.Interfaces;

namespace SpicyDinosaur.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SkillsController : ControllerBase
    {
        private readonly ISkillsRepository skillsRepository;

        public SkillsController(ISkillsRepository skillsRepository)
        {
            this.skillsRepository = skillsRepository;
        }

        // GET api/values
        [HttpGet]
        public async Task<IActionResult> GetSkillCategories()
        {
            var categories = await skillsRepository.GetSkillsByCategory();
            return Ok(categories);
        }

        // POST api/values
        [HttpPost]
        public async Task<IActionResult> CreateSkillCategory([FromBody] SkillCategory value)
        {
            var category = await skillsRepository.CreateSkillCategory(value);
            return Ok();
        }


    }
}
