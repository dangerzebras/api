﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SpicyDinosaur.Models;
using SpicyDinosaur.Repository.Interfaces;
using SpicyDinosaur.Services.Interfaces;

namespace SpicyDinosaur.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OpportunityController : ControllerBase
    {
        private readonly ISkillsRepository skillsRepository;
        private readonly ISkillService skillService;

        public OpportunityController(ISkillsRepository skillsRepository, ISkillService skillService)
        {
            this.skillsRepository = skillsRepository;
            this.skillService = skillService;
        }

        // GET api/Opportunity
        [HttpGet]
        public async Task<IActionResult> GetOpportunities()
        {
            var opportunities = await skillsRepository.GetOpportunities();
            return Ok(opportunities);
        }

        // POST api/Opportunity
        [HttpPost]
        public async Task<IActionResult> CreateOpportunity([FromBody] NewOpportunity value)
        {
            var opportunity = await skillService.CreateOpportunity(value);
            return Ok(opportunity);
        }
    }
}
