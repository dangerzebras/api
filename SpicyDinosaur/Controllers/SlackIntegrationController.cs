﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using SpicyDinosaur.Models;
using SpicyDinosaur.Models.SlackResponses;
using SpicyDinosaur.Repository.Interfaces;
using SpicyDinosaur.Services.Interfaces;

namespace SpicyDinosaur.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SlackIntegrationController : ControllerBase
    {
        private readonly IUserService userService;
        private readonly ISlackRequestRepository slackRequestRepository;
        private readonly ISkillsRepository skillsRepository;
        private readonly ISlackService slackService;
        public SlackIntegrationController(
            IUserService userService, 
            ISlackRequestRepository slackRequestRepository, 
            ISkillsRepository skillsRepository, 
            ISlackService slackService)
        {
            this.userService = userService;
            this.slackRequestRepository = slackRequestRepository;
            this.skillsRepository = skillsRepository;
            this.slackService = slackService;
        }

        [HttpPost]
        public IActionResult FromSlackEndorse([FromForm] SlashCommandPayload value)
        {
            if(value != null)
            {
                slackRequestRepository.AddLog(value);
                if (value.text == null)
                {
                    slackService.SendHelpText(value.response_url);
                    return Ok();
                } else if (value.text.ToLower() == "help")
                {
                    slackService.SendHelpText(value.response_url);
                    return Ok();
                } else
                {
                    userService.Endorse(value.user_id, value.user_name, value.text, value.response_url);
                }
                return Ok("Hold on, we are working...");
            }
            return Ok("Something went wrong, we were unable to save your endorsement.");
        }

        [HttpPost]
        [Route("Endorsements")]
        public IActionResult FromSlackSkills([FromForm] SlashCommandPayload value)
        {
            if (value.text != null && value.text.ToLower() == "help")
            {
                slackService.SendHelpText(value.response_url);
                return Ok();
            }
            slackService.SendUserSkills(value.user_id, value.response_url);
            return Ok();
        }

        [HttpPost]
        [Route("Opportunity")]
        [Produces("application/json")]
        public async Task<IActionResult> FromSlackOpportunities([FromForm] SlashCommandPayload value)
        {
            slackRequestRepository.AddLog(value);
            if (value.text != null && value.text.ToLower() == "help")
            {
                slackService.SendHelpText(value.response_url);
                return Ok();
            }
            Int32.TryParse(value.text, out int count);
            var opportunities = await skillsRepository.GetOpportunities(count);
            SlackResponse response = await BuildOpportunitiesSlackResponse(opportunities, value.user_id);
            return Ok(response);
        }

        [HttpPost]
        [Route("Interest")]
        public async Task<IActionResult> FromSlackInterest([FromForm] SlackMessageActionResponse value)
        {
            var payload = JsonConvert.DeserializeObject<Payload>(value.payload);
            var userId = payload.User.Id;
            if (payload.Actions != null && payload.Actions.Count > 0)
            {
                string opportunityId = payload.Actions[0].Value;
                Opportunity opportunity = await skillsRepository.UpdateOpportunity(userId.ToString(), opportunityId);
                return Ok($"Your interest in *{opportunity.Title}* has been saved.");
            }
            return Ok("Something went wrong.");
        }

        private async Task<SlackResponse> BuildOpportunitiesSlackResponse(IList<Opportunity> opportunities, string userId)
        {
            SlackResponse response = new SlackResponse
            {
                Text = "Current engagement opportunities",
                Response_type = ResponseType.ephemeral,
                Attachments = new List<Attachment>()
            };
            foreach (Opportunity opportunity in opportunities)
            {
                IList<string> skillNames = new List<string>();
                if (opportunity.SkillIds != null)
                {
                    skillNames = await skillsRepository.GetSkillNamesByIds(opportunity.SkillIds);
                }
                Field descriptionField = new Field
                {
                    Title = "Description",
                    Value = opportunity.Description,
                    Short = false
                };
                Field skillsField = new Field
                {
                    Title = "Skills",
                    Value = String.Join(", ", skillNames),
                    Short = false
                };
                Models.Action action = new Models.Action
                {
                    Name = "select",
                    Text = "I'm Interested!",
                    Type = "button",
                    Value = opportunity.Id
                };
                Attachment attachment = new Attachment
                {
                    Fields = new List<Field>(),
                    Actions = new List<Models.Action>(),
                    Callback_id = "abc"
                };

                if (opportunity.UserIds != null && opportunity.UserIds.Contains(userId))
                {
                    attachment.Color = "#3AA3E3"; // blue
                }

                attachment.Fields.Add(descriptionField);
                attachment.Fields.Add(skillsField);
                attachment.Actions.Add(action);
                attachment.Title = opportunity.Title;
                response.Attachments.Add(attachment);
            }
            return response;
        }
    }
}